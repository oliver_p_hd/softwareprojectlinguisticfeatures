# Installation  

* Install Anaconda https://www.continuum.io/downloads
### Command-Line 
* conda update conda
* conda install -c https://conda.anaconda.org/spacy spacy
* python -m spacy.en.download


### Generate Data
* run main.py
* run TopNToCSV.py