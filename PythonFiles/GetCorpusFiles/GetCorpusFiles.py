#-*- coding: utf-8 -*-
from os import walk, path

"""
Returns a tuple-list (Tuple 1: Absolute path to file, Tuple 2: Only filename) 
of all files in the given directory that matches the selection. 
If the selection is empty, all txt-Files will be returned. 
Example: if selection = ['test', 'GER', 'medium', 'P1']
Output:
('../../Corpus/toefl11_tokenised/test_P1_GER_medium_1046064.txt', 'test_P1_GER_medium_1046064')
('../../Corpus/toefl11_tokenised/test_P1_GER_medium_1054917.txt', 'test_P1_GER_medium_1054917')
('../../Corpus/toefl11_tokenised/test_P1_GER_medium_1079141.txt', 'test_P1_GER_medium_1079141')
('../../Corpus/toefl11_tokenised/test_P1_GER_medium_1080585.txt', 'test_P1_GER_medium_1080585')
('../../Corpus/toefl11_tokenised/test_P1_GER_medium_1082036.txt', 'test_P1_GER_medium_1082036')
('../../Corpus/toefl11_tokenised/test_P1_GER_medium_1164382.txt', 'test_P1_GER_medium_1164382')
"""  
def get_files(corp_dir, selection=[]):
    docutemts_tupleList = list()
    # Find all documents and sub-folders in a directory    
    for root, dirs, files in walk(corp_dir, topdown=False): #@UnusedVariable dirs
        for name in files:
            # We want only txt-files, because our corpus contains only txt-files.
            if name.endswith(".txt"):
                match_conditions = True
                for element in selection:
                    if not element in name:
                        match_conditions = False
                if match_conditions:
                    # name[:-4] to delete '.txt'
                    docutemts_tupleList.append((path.join(root, name),(name[:-4])))
    
    return(docutemts_tupleList)


if __name__ == "__main__":
    print('*** Example ***')
    files = get_files(corp_dir='../../Corpus/toefl11_tokenised', 
                      selection = ['test', 'GER', 'medium', 'P1'])
    for f in files:
        print(f)