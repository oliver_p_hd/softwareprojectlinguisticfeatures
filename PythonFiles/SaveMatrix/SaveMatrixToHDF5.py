#-*- coding: utf-8 -*-
import tables
from numpy import dtype
from os.path import isfile


class SaveMatrixToHdf5():
    def __init__(self, h5_file_path='HDF5Files/feature_sets.h5'):
        self.h5_file_path = h5_file_path
        self.numeral_prefix = ['nulli', 'uni', 'bi', 'tri', 'quadri', 'quinque', 'sexa', 'septi', 'octo', 'novem']
    
    
    
    def save_matrix(self, matrix, node, name, n_gram):
        print(' - Saving Matrix "{0}" with shape: {1}'.format(name, matrix.shape))
        h5file = tables.open_file(self.h5_file_path, 'a')
        # delete array before add new array
        try:
            h5file.remove_node(node.format(self.numeral_prefix[n_gram]), name)
        except:
            pass
       
        if matrix.dtype == dtype('O'):
            # for strings 
            atom = tables.StringAtom(itemsize=60)
        else:
            atom = tables.Atom.from_dtype(matrix.dtype)
        # filters=tables.Filters(9, 'blosc') is the compression 
        ds = h5file.create_carray(node.format(self.numeral_prefix[n_gram]), name, atom, matrix.shape, filters=tables.Filters(9, 'blosc'))
        ds[:] = matrix
        
        h5file.close()    
        
        
    def create_hdf5_fileHierarchy(self):
        '''
        We need to create the hdf5 file hierarchy before we are able to store our arrays. 
        Every new set of features needs to be add here. 
        At the moment it supports uni to novem nGrams for word, character and POS. 
        '''
        numeral_prefix = ['uni', 'bi', 'tri', 'quadri', 'quinque', 'sexa', 'septi', 'octo', 'novem']
        
        if not isfile(self.h5_file_path):
            h5file = tables.open_file('HDF5Files/feature_sets.h5', 'w')
            
            h5file.create_group('/', 'feature_sets', 'desc')
            h5file.create_group('/feature_sets', 'word_ngrams', 'desc wng')
            h5file.create_group('/feature_sets', 'character_ngram', 'desc cng')
            h5file.create_group('/feature_sets', 'pos_ngram', 'desc cng')
            
            for pre in numeral_prefix:
                h5file.create_group('/feature_sets/word_ngrams', '{0}_gram'.format(pre), 'desc w_bi_g')    
                h5file.create_group('/feature_sets/character_ngram', '{0}_gram'.format(pre), 'desc c_tri_g')
                h5file.create_group('/feature_sets/pos_ngram', '{0}_gram'.format(pre), 'desc pos_bi_g')
            
            h5file.close()
    
    

if __name__ == "__main__":
    s = SaveMatrixToHdf5()