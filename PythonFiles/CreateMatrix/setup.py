from distutils.core import setup
from distutils.extension import Extension
from Cython.Distutils import build_ext as build_pyx
 
setup(name = 'log_cython', ext_modules=[Extension('log_cython', ['log_cython.pyx'])], cmdclass = { 'build_ext': build_pyx })
