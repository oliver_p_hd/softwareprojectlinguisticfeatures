#-*- coding: utf-8 -*-
import numpy as np
from math import log, log1p
from numba import jit
from collections import OrderedDict
from sklearn.feature_extraction.text import TfidfTransformer

"""
Create ordered (high to low based on the Frequency Matrix): 
 - Frequency Matrix
 - TfIdf Matrix
 - Log-Entropy-Weight Matrix
"""

 

def create_freq_matrix(ft_dict):
    '''
    Creeate out of the a feature dictonary an ordered (column sum) frequency matrix.
    '''
    print(' - Create Frequency-Matrix')            
    row_doc_name_dict, col_ft_dict = __build_matrix_header(ft_dict)
    __test_dict_row_indices(row_doc_name_dict, col_ft_dict)
    np_freq_matrix, np_row_doc_name, np_col_ft = __fill_matrix(ft_dict, row_doc_name_dict, col_ft_dict)
    np_freq_matrix_ordered, np_col_ft_ordered, np_sum_o = __order_matrix(np_freq_matrix, np_col_ft)

    return(np_freq_matrix_ordered, np_row_doc_name, np_col_ft_ordered, np_sum_o)
    
    
def __build_matrix_header(ft_dict):
    '''
    To keep the numpy array efficient, we have to store the feature labels (names)
    and informations about the document (.txt names), like 'SUBSET', 'PROMPT', 'L1', 'PROFICIENCY', 'ESSAYID'
    somewhere else. For that we use pythons OrderedDict to keep the order the same. 
    '''
    row_doc_name_dict = OrderedDict()
    col_ft_dict = OrderedDict()
    
    c = 0
    for i, k in enumerate(ft_dict.keys()):
        row_doc_name_dict[k] = i
        for k in ft_dict[k].keys():
            if k in col_ft_dict:
                continue
            else:
                col_ft_dict[k] = c
                c += 1

    return(row_doc_name_dict, col_ft_dict)


def __test_dict_row_indices(row_doc_name_dict, col_ft_dict):
    '''
    For debugging purpose
    '''
    for i, k in enumerate(row_doc_name_dict.keys()):
        if not row_doc_name_dict[k] == i:
            print('Wrong!!!!!!!!')
            print(row_doc_name_dict[k], i)

    for i, k in enumerate(col_ft_dict.keys()):
        if not col_ft_dict[k] == i:
            print('Wrong++++++')
            print(col_ft_dict[k], i)
        
        
def __fill_matrix(ft_dict, row_doc_name_dict, col_ft_dict):
    print(' - Fill Matrix')    
    np_freq_matrix = np.zeros((len(row_doc_name_dict), len(col_ft_dict)), dtype='uint16') 
    
    np_col_ft = np.zeros((1, len(col_ft_dict)), dtype=object) 
    for i, ft in enumerate(col_ft_dict): 
        np_col_ft[0, i] = ft
    
    np_row_doc_name = np.zeros((len(row_doc_name_dict),5), dtype=object)
    for i, doc in enumerate(row_doc_name_dict):
        np_row_doc_name[i, :] = doc.split('_')
    
    for key in ft_dict.keys():
        for k in ft_dict[key].keys():
            np_freq_matrix[row_doc_name_dict[key], col_ft_dict[k]] = ft_dict[key][k]
    
    return(np_freq_matrix, np_row_doc_name, np_col_ft)


def __order_matrix(np_freq_matrix, np_col_ft):
    print(' - Order Matrix')
    np_sum = np_freq_matrix.sum(0)
    np_sum_o = (-np_sum).argsort()
    np_freq_matrix_ordered = np.take(np_freq_matrix, np_sum_o, axis=1) 
    np_col_ft_ordered = np.take(np_col_ft, np_sum_o, axis=1) 
    
    np_ft_sum_o = np.take(np_sum, np_sum_o)

    return(np_freq_matrix_ordered, np_col_ft_ordered, np_ft_sum_o)  


def create_tfidf_matrix(np_freq_matrix_ordered):
    """
    http://blog.christianperone.com/2011/10/machine-learning-text-feature-extraction-tf-idf-part-ii/
    """
    print(' - Create TF-IDF-Matrix')
    tfidf = TfidfTransformer(norm="l2")
    tfidf.fit(np_freq_matrix_ordered)
    np_tfidf_matrix = tfidf.transform(np_freq_matrix_ordered)
        
    return(np_tfidf_matrix.todense())

    
#@profile
# Speed up numpy / python code
@jit('float32[:,:](uint16[:,:])', nogil=True)
def create_log_entropy_weight_matrix(np_freq_matrix_ordered):
    print(' - Create Log-Entropy-Weight-Matrix')
    np_p_ij_matrix = create_np_p_ij_matrix_forLEW(np_freq_matrix_ordered)
    np_p_ij_matrix_sum = np_p_ij_matrix.sum(0)
    np_log_entropy_weight_matrix = np.zeros(np_freq_matrix_ordered.shape, dtype='float32')
    n_doc = int(np_freq_matrix_ordered.shape[0])
    rows = np_freq_matrix_ordered.shape[0]
    #negative_value = False
    n_doc_log = log1p(n_doc)

    for c, np_p_ij_matrix_sum_i in enumerate(np_p_ij_matrix_sum):
        for r in range(rows):
            local_weight_i = log1p(np_freq_matrix_ordered[r, c])
            if not np_p_ij_matrix[r, c]:
                np_log_entropy_weight_matrix[r, c] = local_weight_i
            else:                
                global_weight_i = 1 + (np_p_ij_matrix_sum_i / n_doc_log)
                np_log_entropy_weight_matrix[r, c] = local_weight_i * global_weight_i

    return(np_log_entropy_weight_matrix)


#@profile
@jit('float32[:,:](uint16[:,:])', nogil=True)
def create_np_p_ij_matrix_forLEW(np_freq_matrix_ordered):
    np_freq_matrix_ordered_sum = np_freq_matrix_ordered.sum(0)
    np_p_ij_matrix = np.zeros(np_freq_matrix_ordered.shape, dtype='float32')
    rows = np_freq_matrix_ordered.shape[0]
    
    for c, freq_sum_i in enumerate(np_freq_matrix_ordered_sum):
        for r in range(rows):
            p_ij = division_lew(np_freq_matrix_ordered[r, c], freq_sum_i)
            if p_ij:
                np_p_ij_matrix[r, c] = p_ij * log(p_ij)
    return(np_p_ij_matrix)


#@profile
@jit('float32[:,:](uint16[:,:])', nogil=True)
def create_log_entropy_weight_matrix_split(np_freq_matrix_ordered):
    '''
    Same as create_log_entropy_weight_matrix only that it holds one column at a time.
    Lowers memory requirement, but is a little bit slower. 
    '''
    print(' - Create Log-Entropy-Weight-Matrix-Test')
    np_log_entropy_weight_matrix = np.zeros(np_freq_matrix_ordered.shape, dtype='float32')
    n_doc = int(np_freq_matrix_ordered.shape[0])
    n_doc_log = log1p(n_doc)
    rows = np_freq_matrix_ordered.shape[0]
    
    for col in range(np_freq_matrix_ordered.shape[1]):
        np_p_ij_col, np_p_ij_col_sum = create_np_p_ij_matrix_forLEW_split(np_freq_matrix_ordered[:,col])
        np_log_entropy_weight_matrix[:,col] = lew_col_n(np_freq_matrix_ordered[:,col], np_p_ij_col, np_p_ij_col_sum, rows, n_doc_log)

    return(np_log_entropy_weight_matrix)

@jit(nogil=True)
def lew_col_n(np_freq_matrix_col_n, np_p_ij_col, np_p_ij_col_sum, rows, n_doc_log):
    '''
    For create_log_entropy_weight_matrix_split
    '''
    np_log_entropy_weight_col_n = np.zeros(np_freq_matrix_col_n.shape, dtype='float32')
    
    for r in range(rows):
        local_weight_i = log1p(np_freq_matrix_col_n[r])
        if not np_p_ij_col[r]:
            np_log_entropy_weight_col_n[r] = local_weight_i
        else:                
            global_weight_i = 1 + (np_p_ij_col_sum / n_doc_log)
            np_log_entropy_weight_col_n[r] = local_weight_i * global_weight_i
        return(np_log_entropy_weight_col_n[:])


#@profile
@jit('float32[:](uint16[:])', nogil=True)
def create_np_p_ij_matrix_forLEW_split(np_freq_matrix_col_n):
    '''
    For create_log_entropy_weight_matrix_split
    '''
    freq_sum_i = np_freq_matrix_col_n.sum(0)
    np_p_ij_col = np.zeros(np_freq_matrix_col_n.shape[0], dtype='float32')
    
    for c, freq in enumerate(np_freq_matrix_col_n):
        p_ij = division_lew(freq, freq_sum_i)
        if p_ij:
            np_p_ij_col[c] = p_ij * log(p_ij)
    np_p_ij_col_sum = np_p_ij_col.sum(0)
    return(np_p_ij_col, np_p_ij_col_sum)
    

@jit('float64(uint16, uint64)', cache=True, nopython=True, nogil=True)
def division_lew(x, y):
    return(x / y)


@jit('float64(float32, float64)', cache=True, nopython=True, nogil=True)
def division_lew_f(x, y):
    return(1 + x / y)
    
    
if __name__ == "__main__":
    print(' === TODO === ')