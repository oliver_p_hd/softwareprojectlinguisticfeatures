#-*- coding: utf-8 -*-
from nltk.corpus import stopwords
from nltk.stem import LancasterStemmer
from re import compile

"""
Simply for pre-processing a tokenized sentence. 
"""


re_pat = compile('.*([a-z]|[0-9])+')
stopwords = stopwords.words('english')
wordStemmer = LancasterStemmer()


def remove_stopword(tokenized_sentence, stopWordList=None):
    if stopWordList:
        stopwords = stopWordList
        
    noStopwordsTokenizedSentence = [w for w in tokenized_sentence if w.lower() not in stopwords]    
    return(noStopwordsTokenizedSentence)

    
def do_wordstemm(tokenized_sentence):
    stemmedTokenizedSentence = [wordStemmer.stem(word) for word in tokenized_sentence]          
    return(stemmedTokenizedSentence)


def do_lowercase(tokenized_sentence):
    lowerTokenizedSentence = [word.lower() for word in tokenized_sentence]    
    return(lowerTokenizedSentence)


def remove_puntuation(tokenized_sentence):
    tokenized_sentence = [i for i in tokenized_sentence if not re_pat.match(i)]        
    return(tokenized_sentence)


def remove_puntuation_ngram(tokenized_sentence):
    tokenized_sentence = [i if re_pat.match(i) else '!' for i in tokenized_sentence]        
    return(tokenized_sentence)
    
    
    
if __name__ == "__main__":
    print(' === TODO === ')