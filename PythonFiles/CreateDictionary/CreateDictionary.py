#-*- coding: utf-8 -*-
from nltk.util import ngrams

"""
Out of the given 'files' (tuples) it creates a dictionary ('ngram_dict') where 
the keys the filenames are and the values another dictionary is, wher
the keys the nGram names are and the values the counts in the given file are. 
Example:
{'test_P1_ARA_medium_1106019': {'a-d': 1, 'b-g': 1, 'f-e': 1, 'a-c': 1, 'd-f': 1, 'g-h': 1, 'c-a': 1, 'b-c': 1, 'c-b': 1, 'e-h': 1, 'a-b': 1}, 
'test_P1_ARA_low_1033177': {'dhd-e': 1, 'e-f': 1, 'cc-dhd': 1, 'b-cc': 1, 'd-f': 1, 'c-d': 1, 'b-c': 1, 'f-g': 1, 'a-b': 1}}

n sets the number of grams.

line_preprocess is a method / function that for example removes punctuation, does lower_case,... 

'uni_word_dict' for debugging purpose.  
"""
def create_ngram_dict(files, n, line_preprocess):
    print(' - Create N-Gram-Dictionary')
    ngram_dict = dict()
    uni_word_dict = dict()
    
    for f_tpl in files:
        doc = open(f_tpl[0], 'rU', encoding='utf-8')
        __add_fileName_to_dict(ngram_dict, f_tpl[1])
        for line in doc:
            line = line_preprocess(line)
            grams = ngrams(line, n)
            __add_gram_to_dict(grams, ngram_dict, f_tpl[1])
            for word in line:
                if not word in uni_word_dict:
                    uni_word_dict[word] = None

    return(ngram_dict, uni_word_dict)


def __add_fileName_to_dict(ngram_dict, file_name):
    if not file_name in ngram_dict:
        ngram_dict[file_name] = {}
            

def __add_gram_to_dict(grams, ngram_dict, file_name):
    # '!' is set to ignore a nGram. For example if there are two words separated by a comma.
    for g in grams:
        if '!' in g:
            continue
        g = '-'.join(g)
        if g in ngram_dict[file_name]:
            ngram_dict[file_name][g] += 1
        else:
            ngram_dict[file_name][g] = 1
            
                

if __name__ == "__main__":
    print(' === TODO === ')