#-*- coding: utf-8 -*-
from PythonFiles.SentencePreprocessing.SentencePreprocessing import do_lowercase, remove_puntuation_ngram
from PythonFiles.CreateMatrix.CreateMatrix import create_freq_matrix, create_tfidf_matrix, create_log_entropy_weight_matrix_split
from PythonFiles.CreateDictionary.CreateDictionary import create_ngram_dict
from PythonFiles.SaveMatrix.SaveMatrixToHDF5 import SaveMatrixToHdf5 
import time


class CharacterNGramExtraction():
    def __init__(self):
        pass
        
        
    def create_matrices(self, files, n):
        print('\n * Starting Character-N-Gram Extraction')
        start_time = time.time()
        
        self.to_hdf5 = SaveMatrixToHdf5()
        
        ngram_dict = create_ngram_dict(files, n, self.__line_preprocess)[0]
        np_freq_matrix_ordered, np_col_ft_ordered_shape = self.__build_freq_matrix(ngram_dict, n)
        
        self.__build_bool_matrix(np_freq_matrix_ordered, n)
        
        self.__build_tfidf_matrix(np_freq_matrix_ordered, n)
        
        self.__build_log_entropy_weight_matrix(np_freq_matrix_ordered, n)
        
        print(' * Duration: {0} s.'.format(time.time() - start_time))
        print(' * Numpy-Columns-Features-Ordered: {0}\n'.format(np_col_ft_ordered_shape))
        print('=' * 50); print('=' * 50)
        
        
    
    def __build_freq_matrix(self, ngram_dict, n):
        np_freq_matrix_ordered, np_row_doc_name, np_col_ft_ordered, np_sum_o = create_freq_matrix(ngram_dict)
        self.to_hdf5.save_matrix(np_freq_matrix_ordered, '/feature_sets/character_ngram/{0}_gram', 'feature_frequency_matrix_ordered', n)
        self.to_hdf5.save_matrix(np_freq_matrix_ordered.astype(bool), '/feature_sets/character_ngram/{0}_gram', 'feature_bool_matrix_ordered', n)
        self.to_hdf5.save_matrix(np_row_doc_name, '/feature_sets/character_ngram/{0}_gram', 'np_row_doc_name', n)
        self.to_hdf5.save_matrix(np_col_ft_ordered, '/feature_sets/character_ngram/{0}_gram', 'np_col_ft_ordered', n)
        self.to_hdf5.save_matrix(np_sum_o, '/feature_sets/character_ngram/{0}_gram', 'np_ft_sum_ordered', n)
        return(np_freq_matrix_ordered, np_col_ft_ordered.shape)
    
    
    def __build_bool_matrix(self, np_freq_matrix_ordered, n):
        self.to_hdf5.save_matrix(np_freq_matrix_ordered.astype(bool), '/feature_sets/word_ngrams/{0}_gram', 'feature_bool_matrix_ordered', n)
    
    def __build_tfidf_matrix(self, np_freq_matrix_ordered, n):
        numpy_tfidf_matrix_ordered = create_tfidf_matrix(np_freq_matrix_ordered)
        self.to_hdf5.save_matrix(numpy_tfidf_matrix_ordered, '/feature_sets/character_ngram/{0}_gram', 'numpy_tfidf_matrix_ordered', n)
    
    
    def __build_log_entropy_weight_matrix(self, np_freq_matrix_ordered, n):
        numpy_logEntropyWeight_matrix_ordered = create_log_entropy_weight_matrix_split(np_freq_matrix_ordered)
        self.to_hdf5.save_matrix(numpy_logEntropyWeight_matrix_ordered, '/feature_sets/character_ngram/{0}_gram', 'numpy_logEntropyWeight_matrix_ordered', n)
    

    def __line_preprocess(self, line):
        line = line.split()
        line = do_lowercase(line)
        line = remove_puntuation_ngram(line)
        line = '_'.join(line)
        line = list(line)
        return(line)

                
    

if __name__ == "__main__":
    print(' === TODO === ')
