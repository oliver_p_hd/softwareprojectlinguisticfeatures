#-*- coding: utf-8 -*-
import tables
import pandas
import numpy

'''
It was meant to be for the group (other members) so they could be able to load data in R.
'''
class CreateTopNCSV():
    def __init__(self):
        topn = 100
        
        #word_bi_gram
        self.getTopN('/feature_sets/word_ngrams/bi_gram', '/feature_frequency_matrix_ordered', topn)
        self.getTopN('/feature_sets/word_ngrams/bi_gram', '/feature_bool_matrix_ordered', topn)
        self.getTopN('/feature_sets/word_ngrams/bi_gram', '/numpy_tfidf_matrix_ordered', topn)
        self.getTopN('/feature_sets/word_ngrams/bi_gram', '/numpy_logEntropyWeight_matrix_ordered', topn)
        
        #char_bi_gram
        self.getTopN('/feature_sets/character_ngram/bi_gram', '/feature_frequency_matrix_ordered', topn)
        self.getTopN('/feature_sets/character_ngram/bi_gram', '/numpy_tfidf_matrix_ordered', topn)
        self.getTopN('/feature_sets/character_ngram/bi_gram', '/numpy_logEntropyWeight_matrix_ordered', topn)
        
        #pos_bi_grams
        self.getTopN('/feature_sets/pos_ngram/bi_gram', '/feature_frequency_matrix_ordered', topn)
        self.getTopN('/feature_sets/pos_ngram/bi_gram', '/feature_bool_matrix_ordered', topn)
        self.getTopN('/feature_sets/pos_ngram/bi_gram', '/numpy_tfidf_matrix_ordered', topn)
        self.getTopN('/feature_sets/pos_ngram/bi_gram', '/numpy_logEntropyWeight_matrix_ordered', topn)
        
        
        self.getTopN('/feature_sets/word_ngrams/tri_gram', '/feature_frequency_matrix_ordered', topn)
        self.getTopN('/feature_sets/word_ngrams/tri_gram', '/feature_bool_matrix_ordered', topn)
        self.getTopN('/feature_sets/word_ngrams/tri_gram', '/numpy_tfidf_matrix_ordered', topn)
        self.getTopN('/feature_sets/word_ngrams/tri_gram', '/numpy_logEntropyWeight_matrix_ordered', topn)
        
        #char_bi_gram
        self.getTopN('/feature_sets/character_ngram/tri_gram', '/feature_frequency_matrix_ordered', topn)
        self.getTopN('/feature_sets/character_ngram/tri_gram', '/feature_bool_matrix_ordered', topn)
        self.getTopN('/feature_sets/character_ngram/tri_gram', '/numpy_tfidf_matrix_ordered', topn)
        self.getTopN('/feature_sets/character_ngram/tri_gram', '/numpy_logEntropyWeight_matrix_ordered', topn)
        
        #pos_bi_grams
        self.getTopN('/feature_sets/pos_ngram/tri_gram', '/feature_frequency_matrix_ordered', topn)
        self.getTopN('/feature_sets/pos_ngram/tri_gram', '/feature_bool_matrix_ordered', topn)
        self.getTopN('/feature_sets/pos_ngram/tri_gram', '/numpy_tfidf_matrix_ordered', topn)
        self.getTopN('/feature_sets/pos_ngram/tri_gram', '/numpy_logEntropyWeight_matrix_ordered', topn)
        
        
        print('done')
        
    
    def getTopN(self, node, table, topn):
        h5file = tables.open_file('HDF5Files/feature_sets.h5', 'r')
        
        df_feature_matrix = self.getFeatureMatrixFromHDF5(h5file, node, table, topn)
        df_ngramsHeader = self.getfeatureLablesFromHDF5(h5file, node, topn)
        df_document_matrix = self.getDocMatricFromHDF5(h5file, node, topn)
        
        mergedDocAndFeature = pandas.concat([df_document_matrix, df_feature_matrix], axis=1)
        finalMatrix = pandas.concat([df_ngramsHeader , mergedDocAndFeature])
        node_split = node.split('/')
        finalMatrix.to_csv('CSVFiles' + '/' + node_split[2] + '_' + node_split[3] + '_' + table[1:] + '.csv', sep=',', encoding='utf-8', index=False, header=False)
        
        h5file.close()
        
        
    def getFeatureMatrixFromHDF5(self, h5file, node, table, topn):
        feature_matrix = h5file.get_node(node + table)
        numpy_feature_matrix = feature_matrix[:,:topn]
        df_feature_matrix = pandas.DataFrame(numpy_feature_matrix, columns=[i for i in range(5, numpy_feature_matrix.shape[1]+5)])
        return(df_feature_matrix)
        
        
    def getfeatureLablesFromHDF5(self, h5file, node, topn):
        ngramsHeader_matrix = h5file.get_node(node + '/np_col_ft_ordered')
        np_col_ft_ordered = ngramsHeader_matrix[:,:topn]
        np_columns_features_ordered = numpy.zeros((1, np_col_ft_ordered.shape[1] + 5), dtype=object)
        np_columns_features_ordered[0][0] = 'SUBSET'
        np_columns_features_ordered[0][1] = 'PROMPT'
        np_columns_features_ordered[0][2] = 'L1'
        np_columns_features_ordered[0][3] = 'PROFICIENCY'
        np_columns_features_ordered[0][4] = 'ESSAYID'
        for x in range(np_col_ft_ordered.shape[0]):
            for y in range(np_col_ft_ordered.shape[1]):
                np_columns_features_ordered[x][y+5] = np_col_ft_ordered[x][y].decode('utf8')
        df_ngramsHeader = pandas.DataFrame(np_columns_features_ordered)
        return(df_ngramsHeader)
    
    
    def getDocMatricFromHDF5(self, h5file, node, topn):
        document_matrix = h5file.get_node(node + '/np_row_doc_name')
        numpy_document_matrix = document_matrix[:]
        np_document_matrix = numpy.zeros(numpy_document_matrix.shape, dtype=object)
        for x in range(document_matrix.shape[0]):
            for y in range(document_matrix.shape[1]):
                np_document_matrix[x][y] = document_matrix[x][y].decode('utf8')
        df_document_matrix = pandas.DataFrame(np_document_matrix)
        return(df_document_matrix)
    

if __name__ == "__main__":
    c = CreateTopNCSV()