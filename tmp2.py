#-*- coding: utf-8 -*-
import numpy as np
import math
from numba import jit, vectorize, float64, float32, uint8, uint64
from collections import OrderedDict
from sklearn.feature_extraction.text import TfidfTransformer



class CreateMatrix():
    def __init__(self):
        pass
    

    def create_freq_matrix(self, dict_ft):
        print(' * Create Frequency-Matrix')            
        dicto_row_doc_name, dicto_col_ft = self.__build_matrix_header(dict_ft)
        self.__test_dict_row_col_indices(dicto_row_doc_name, dicto_col_ft)
        np_freq_matrix, np_row_doc_name, np_col_ft = self.__fill_matrix(dict_ft, dicto_row_doc_name, dicto_col_ft)
        np_freq_matrix_ordered, np_col_ft_ordered = self.__order_matrix_by_col(np_freq_matrix, np_col_ft)

        return(np_freq_matrix_ordered, np_row_doc_name, np_col_ft_ordered)
        
        
    def __build_matrix_header(self, dict_ft):
        c_col_i = 0
        dicto_row_doc_name = OrderedDict()
        dicto_col_ft = OrderedDict()
        
        for i, key in enumerate(dict_ft.keys()):
            dicto_row_doc_name[key] = i
            for k in dict_ft[key].keys():
                if k in dicto_col_ft:
                    pass
                else:
                    dicto_col_ft[k] = c_col_i
                    c_col_i += 1
                
        return(dicto_row_doc_name, dicto_col_ft)
    
    
    def __test_dict_row_col_indices(self, dicto_row_doc_name, dicto_col_ft):
        for i, key in enumerate(dicto_row_doc_name.keys()):
            if not dicto_row_doc_name[key] == i:
                print('Wrong!!!!!!!!')
                print(dicto_row_doc_name[key], i)
   
        for i, key in enumerate(dicto_col_ft.keys()):
            if not dicto_col_ft[key] == i:
                print('Wrong++++++')
                print(dicto_col_ft[key], i)
            
            
    def __fill_matrix(self, dict_ft, dicto_row_doc_name, dicto_col_ft):
        print(' * TODO Fill Matrix')
        
        np_freq_matrix = np.zeros((len(dicto_row_doc_name), len(dicto_col_ft)), dtype=np.uint8) 
        np_col_ft = np.zeros((1, len(dicto_col_ft)), dtype=object) 
        for i, ft in enumerate(dicto_col_ft):
            np_col_ft[0][i] = ft
        
        np_row_doc_name = np.zeros((len(dicto_row_doc_name),5), dtype=object)
        for i, doc in enumerate(dicto_row_doc_name):
            doc = doc.split('_')
            np_row_doc_name[i][0] = doc[0]
            np_row_doc_name[i][1] = doc[1]
            np_row_doc_name[i][2] = doc[2]
            np_row_doc_name[i][3] = doc[3]
            np_row_doc_name[i][4] = doc[4]
        
        for key in dict_ft.keys():
            for k in dict_ft[key].keys():
                np_freq_matrix[dicto_row_doc_name[key]][dicto_col_ft[k]] = dict_ft[key][k]
        
        return(np_freq_matrix, np_row_doc_name, np_col_ft)
    
    
    def __order_matrix_by_col(self,np_freq_matrix, np_col_ft):
        print(' * Order Matrix')        
        np_sum = np_freq_matrix.sum(0)
        np_sum_o = (-np_sum).argsort()
        np_freq_matrix_ordered = np.take(np_freq_matrix, np_sum_o, axis=1) 
        np_col_ft_ordered = np.take(np_col_ft, np_sum_o, axis=1) 

        return(np_freq_matrix_ordered, np_col_ft_ordered)  


    def create_tfidf_matrix(self, np_freq_matrix_ordered):
        """
        http://blog.christianperone.com/2011/10/machine-learning-text-feature-extraction-tf-idf-part-ii/
        """
        print(' * Create TF-IDF-Matrix')
        tfidf = TfidfTransformer(norm="l2")
        tfidf.fit(np_freq_matrix_ordered)
        np_tfidf_matrix = tfidf.transform(np_freq_matrix_ordered)
            
        return(np_tfidf_matrix.todense())
    
    #@profile
    @jit
    def create_log_entropy_weight_matrix(self, np_freq_matrix_ordered):
        print(' * Create Log-Entropy-Weight-Matrix')
        np_p_ij_matrix = self.__create_np_p_ij_matrix_forLEW(np_freq_matrix_ordered)
        np_p_ij_matrix_sum = np_p_ij_matrix.sum(0)
        np_log_entropy_weight_matrix = np.zeros(np_freq_matrix_ordered.shape, dtype=np.float32)
        n_doc = int(np_freq_matrix_ordered.shape[0])
        row_len, col_len = np_freq_matrix_ordered.shape
        negative_value = False
        n_doc_log = math.log(n_doc + 1)
        '''
        local_weight = np.log(np_freq_matrix_ordered + 1)
        for col_i, np_p_ij_matrix_sum_i in enumerate(np_p_ij_matrix_sum):
            local_weight_j = local_weight[:, col_i]
            ind = np_p_ij_matrix[:, col_i] != 0
            local_weight_j[ind] *= 1 + (np_p_ij_matrix_sum_i / n_doc_log)
            np_log_entropy_weight_matrix[:, col_i] = local_weight_j
            
        '''
        for col_i, np_p_ij_matrix_sum_i in enumerate(np_p_ij_matrix_sum):
            for row_i in range(row_len):
                local_weight_i = math.log(np_freq_matrix_ordered[row_i, col_i] + 1)
                if not np_p_ij_matrix[row_i, col_i]:
                    np_log_entropy_weight_matrix[row_i, col_i] = local_weight_i
                else:                
                    #global_weight_i = division_lew_f(np_p_ij_matrix_sum_i, n_doc_log)
                    global_weight_i = 1 + (np_p_ij_matrix_sum_i / n_doc_log)
                    np_log_entropy_weight_matrix[row_i, col_i] = local_weight_i * global_weight_i
        #        if np_log_entropy_weight_matrix[row_i][col_i] < 0:
        #            negative_value = True
        #print(' - - test negative_value:', negative_value)
        #'''

        return(np_log_entropy_weight_matrix)
    
    #@profile
    @jit
    def __create_np_p_ij_matrix_forLEW(self, np_freq_matrix_ordered):
        np_freq_matrix_ordered_sum = np_freq_matrix_ordered.sum(0)
        np_p_ij_matrix = np.zeros(np_freq_matrix_ordered.shape, dtype=np.float32)
        row_len, col_len = np_freq_matrix_ordered.shape
        
        for col_i, ft_freq_sum_i in enumerate(np_freq_matrix_ordered_sum):
            for row_i in range(row_len):
                p_ij = division_lew(np_freq_matrix_ordered[row_i, col_i], ft_freq_sum_i)
                if p_ij:
                    np_p_ij_matrix[row_i, col_i] = p_ij * math.log(p_ij)

        return(np_p_ij_matrix)
    
#@vectorize(['float64(uint8, uint64)'])
@jit('float64(uint8, uint64)', nopython=True, nogil=True)
def division_lew(x, y):
    return(x / y)


@vectorize(['float64(float32, float64)'])
def division_lew_f(x, y):
    return(1 + x / y)
    
    
if __name__ == "__main__":
    pass