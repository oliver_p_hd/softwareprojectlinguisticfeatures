#-*- coding: utf-8 -*-

"""
Simple script to extract features out of the TOEFL11 corpus.
Add corp_dir, add feature-set-extraction-class like 'WordNGramExtraction()', set number of nGrams.
At the moment there are 
 - WordNGramExtraction
 - CharacterNGramExtraction
 - POSNGramExtraction
 with a possible number of 1-9 nGrams. 
 If you want to change the pre-processing line, for example add stopword-removing,
 then you need to do that in the corresponding extraxtion class in the method / function 'def __line_preprocess(self, line):'.
"""

import time
from PythonFiles.GetCorpusFiles.GetCorpusFiles import get_files
from PythonFiles.FeatureExtractionFiles.WordNGram import WordNGramExtraction
from PythonFiles.FeatureExtractionFiles.CharacterNGram import CharacterNGramExtraction
from PythonFiles.FeatureExtractionFiles.POSNGram import POSNGramExtraction
from PythonFiles.SaveMatrix.SaveMatrixToHDF5 import SaveMatrixToHdf5 

print('=' * 50); print('=' * 50)
start_time = time.time()
save_matrix = SaveMatrixToHdf5()
save_matrix.create_hdf5_fileHierarchy()

files = get_files(corp_dir='Corpus/toefl11_tokenised', selection = ['test'])
#files = get_files(corp_dir = 'Corpus/test', selection = ['test'])

w_gram = WordNGramExtraction()
w_gram.create_matrices(files, n=2)
w_gram.create_matrices(files, n=3)


c_gram = CharacterNGramExtraction()
c_gram.create_matrices(files, n=2)
c_gram.create_matrices(files, n=3)

p_gram = POSNGramExtraction()
p_gram.create_matrices(files, n=2)
p_gram.create_matrices(files, n=3)


print(' == Total Duration: {0} s.'.format(time.time() - start_time))